## Документация по плагину ориентированных модальных окон (PopupMD v.1.0)

Для начала работы нужно подключить все файлы плагина к вашему проекту (Для первой версии это один файл стилей модального окна и один файл скриптов).

Так же стоит учесть баги, в данном плагине они есть и связаны с параметром onlyOne и closeAuto.

Для создания нового модального окна, нужно создать объект класса Popup и передать в него возможные параметры:

`element` - Уникальный атрибут объекта на который ориентируется модальное окно. Передавать нужно какое либо уникальное значение, желательно что бы это был параметр **id**

`data` - Набор параметров модального окна. Передаются параметры в виде объекта, где:
 
`view` : Отображение модального окна _bool(true, false)_ по умолчанию _false_

`contents` :  Уникальный атрибут содержимого, которое нужно вставить в модальное окно _string(*)_

`modalSide` : Сторона с которой будет ориентировано модальное окно _string(right, left, top, bottom)_ по умолчанию _right_

`pointerPosition` : Позиция где будет ориентирована пипка _string(right, left, center)_ по умолчанию _center_

`sizePointerPosition` : Отступ пипки (в px) от центра _int(*)_, если не указан параметр **pointerPosition** отступ нужно указывать от положительного значения до отрицательного, иначе можно сделать реверс

`style` : Набор стилей модального окна. Передаются параметры в виде объекта, что содержит в себе перечень стилей окна. Доступные стили: _color_, _backgroundColor_, _padding_, _borderRadius_

`marginElement` : Отступ модального окна от целевого объекта _int(*)_

`onlyOne` : Закрывать ли модальное окно при вызове другого модального окна (эта функция может быть полезной, когда включен параметр ...не забыть дописать...). _string(true, false)_ по умолчанию: _true_

`cloneContents` : Клонировать ли содержимое модального окна (параметр _contents_) или вырезать. Полезно когда содержимое модального окна должно быть изменено и подгружено в следующий раз, с этими же изменениями, без дополнительных запросов до БД. _string(true, false)_ по умолчанию: _true_

`closeButton` : Уникальный атрибут елемента (кнопки), при клике на который будет закрываться модальное окно. По умолчанию _.modal_close_

`closeAuto` : Закрывать ли модальное окно при кликах вне его границы _bool(true, false)_ по умолчанию _true_

~~~~
P.S. Эта версия ориентированных модальных окон (PopupMD) была сделана на быструю руку, дальнейшее развитие не планировалось
~~~~

**Сайт разработчика: https://dieslog.ru/**

###### Пример подключения и работы:

````
<!doctype html>
<html>
    <head>
        <title>Test</title>
        <link rel="stylesheet" href="popup.css">
        <style>
            html, body{
                margin: 0;
                padding: 0;
                width: 100%;
                height: 100%;
            }
        </style>
    </head>
    <body>
        <div style="width: 100%;">
            <div style="width: 300px; height: 150px; margin: 100px auto; color: #FFFFFF; background: linear-gradient(#FD5C63, #fd4b50); border-radius: 5px 5px 0 5px; padding: 15px; box-shadow: 3px 3px 6px rgba(0,0,0,0.25); display: flex; flex-direction: column; justify-content: space-between;">
                <div style="padding-bottom: 10px; font-size: 16px; border-bottom: 1px solid #D0E4F8; display: flex; justify-content: space-between;">
                    <div contenteditable="true">Title</div>
                    <div style="color: #ffa6aa; cursor: pointer;" class="link_">изменить</div>
                </div>
                <div contenteditable="true">
                    body
                </div>
                <div style="align-self: center;" class="button">
                    <button id="btn" style="border: 1px solid #FFFFFF; color: #FFFFFF; background-color: transparent; padding: 10px; border-radius: 3px; box-shadow: 2px 2px 4px rgba(0,0,0,0.25); outline: none; cursor: pointer;" class="link">
                        click me
                    </button>
                </div>
                <div style="text-align: right; width: inherit; color: #c1121a; align-self: center;">
                    by Maksym Black
                </div>
            </div>
        </div>
        <div class="contents" style="display: none;">etyety</div>
        <div class="view_popup" style="display: none;">
            <div><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam asperiores blanditiis culpa doloribus
                facere facilis fugiat illum iure libero nisi perspiciatis quis rem rerum sapiente sequi, sint unde vel
                    veniam.</p>
                <div style="text-align: center;">
                    <button style="color: #ff524a; border: 1px solid #ff524a; background-color: transparent;" class="btn_close" onclick="clicker()">i`m button</button>
                    <button style="color: #f948ff; border: 1px solid #f948ff; background-color: transparent;" class="btn_close modal_close">i`m close</button>
                </div>
            </div>
        </div>
        <script src="popup.js"></script>
        <script>
            let button = document.querySelector('#btn');
            button.addEventListener('click', function () {
                let popup = new PopupMD('button', {
                    'modalSide' : 'bottom',
                    'view' : true,
                    'contents' : '.view_popup',
                    'pointerPosition' : 'center',
                    'sizePointerPosition' : 80,
                    'style' : {
                        'color' : '#338833',
                        'backgroundColor' : '#33cccc',
                        'padding' : '10px',
                        'borderRadius' : '20px',
                    },
                    'marginElement': 10,
                    'onlyOne': false,
                    'cloneContents': true,
                });
                popup.destroy();
            });

            let link_ = document.querySelector('.link_');
            link_.addEventListener('click', function () {
                let popup = new PopupMD('.link_', {
                    'modalSide' : 'right',
                    'view' : true,
                    'contents' : '.contents',
                    'pointerPosition' : 'right',
                    'sizePointerPosition' : 18,
                    'marginElement': 15,
                    'closeAuto': true,
                });
            });
            function clicker() {
                let popup = new PopupMD('.btn_close', {
                    'modalSide' : 'bottom',
                    'view' : true,
                    'contents' : '.contents',
                    'pointerPosition' : 'center',
                    'cloneContents': false,
                    'closeAuto': false,
                });
            }

        </script>
    </body>
</html>